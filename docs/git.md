# Git

## Git Commit Template

To use a custom template for your git commits you can add a template file into your repository. I like to have some kind of issue number first and then a short title for the commit. If I create a larger commit I always try to provide a list with the changes.

My example `.gitmessage` file in a repository:

```plain
T-0000 – REPLACE THIS TITLE

Changes:
- 
- 
```

To use this template you have to add the following into you `.git/config`:

```
[commit]
    template = .gitmessage
```

If you like to have a template for all your repositories you can add you template to you home folder (`~/.gitmessage`) and add the git configuration into you global git config (`~/.git/config`). But then you have to reference in the git config to the file in you home folder: 

```
[commit]
    template = ~/.gitmessage
```

Source: https://alex-wasik.medium.com/create-a-custom-git-commit-template-84468232a459

## Signing commits

Enable globally to sign every commit: 

```shell
git config --global commit.gpgsign true
```

Sign just a specific commit by adding the `-S` flag:

```shell
git commit -S -m "Foobar"
```

## Git Submodules

### Adding a new submodule

```shell
git submodule add <REPO_PATH> <SUBMODULE_NAME>
```

### Change state of submodule

Just update all submodules to the latest commit:

```shell
git submodule update --remote --recursive
```

Select anything specifically:

```shell
cd <DIR_OF_SUBMODULE>
git fetch

git pull
# OR
git merge origin/master
# OR
git checkout 23e38fa
```

### Remove/Delete a submodule

```shell
git submodule deinit <PATH_TO_SUBMODULE_FOLDER>
git rm <PATH_TO_SUBMODULE_FOLDER>
git commit -m "Remove submodule"
git push
rm -rf .git/modules/<PATH_TO_SUBMODULE_FOLDER>
```
