# Google Chrome - Tips and Tricks

## Domain redirects everytime to https

Chrome redirects sometimes your website request from `http://...` to `https://...`. The reason could be the cached state of the HSTS Header for this domain, which says Chrome for how long it should just use https for all request and not http.

To clear this cached HSTS Header do this:

- Browse to `chrome://net-internals`
- On this site go to the `Domain Security Policy` section in the sidebar
- Enter the domain name under `Delete domain security policies` and press the delete button
