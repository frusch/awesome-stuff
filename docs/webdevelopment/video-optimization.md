# Video optimization for the web

Source: <https://pixelpoint.io/blog/web-optimized-video-ffmpeg/>

Shell Script: <https://gist.github.com/lnikell/7a241aa2fe371170f0fb4a9fb92549ac>

## Embedding

```html
<video>
    <source type="video/webm" src="output.webm" />
    <source type="video/mp4" src="output.mp4" />
</video>
```

## WebM(VP9)

```shell
ffmpeg -i input.mp4 -c:v libvpx-vp9 -crf 40 -vf scale=3840:-2 -deadline best -an output.webm
```

Command flags breakdown:

- `-c:v libvpx-vp9` define which codec to use
- `-crf 40` define settings for Constant Quality mode
- `-vf scale=3840:-2` define settings for scale filter
- `-deadline best` manipulate the speed/quality ration of upcoming compressing
- `-an` remove audio

## MP4(H265)

```shell
ffmpeg -i input.mp4 -c:v libx265 -crf 32 -vf scale=3840:-2 -preset veryslow -tag:v hvc1 -movflags faststart -an output.mp4
```

Command flags breakdown:

`-c:v libx265` define which codec to use
`-crf 32` define settings for Constant Quality mode
`-vf scale=3840:-2` define settings for scale filter
`-preset veryslow` manipulate the speed/quality ration of upcoming compressing
`-tag:v hvc1` make video work on iOS and macOS devices
`-movflags faststart` make your video web-optimized
`-an` remove audio

Adjusting `-crf` values could help to find the best suitable option with a minimal size.

## Troubleshooting

- green screen video during conversion to H.265: try adding the `-pix_fmt yuv420p` flag
- for testing with just a small abount of frames: `-frames:v 1`
