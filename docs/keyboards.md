# Keyboards

- Database of different user specific layouts: <https://keymapdb.com/>
- Home row mod guide: <https://precondition.github.io/home-row-mods>
- Colemak-dh: <https://colemakmods.github.io/mod-dh/>
- nvim config for colemake (not colemak-dh): <https://github.com/theniceboy/nvim>

## Type training websites

- <https://www.keybr.com/>
- <https://monkeytype.com/>
- <https://play.typeracer.com/>
- <https://ranelpadon.github.io/ngram-type/>
- <https://www.typingclub.com/> (Assumes a Qwerty layout)
- <https://www.colemak.academy/> (Also supports other layouts)
- <https://typelit.io> – Typing whie reading literature

## Keyboard configurators

- ZSA Keyboards: <https://configure.zsa.io/>
- QMK: <https://config.qmk.fm/>
  - Cross-platform GUI: <https://get.vial.today/>
