# SSL/TLS

## Check certs of service

```shell
openssl s_client -showcerts -connect <REMOTE_URL>:<REMOTE_PORT>
```

## Creation and issuing CA's and certificates

```
# CA
openssl genrsa -aes256 -out rootCa.key 4096
openssl req -x509 -new -nodes -key rootCa.key -days 3650 -out rootCa.crt -sha512

# certificate
openssl genrsa -out whatever_local.key 4096
openssl req -new -key whatever_local.key -out whatever_local.csr -sha512
openssl x509 -req -in whatever_local.csr -CA rootCa.crt -CAkey rootCa.key -CAcreateserial -out whatever_local.crt -days 365 -sha512
```

## PKCS12 & Keystores
```
## Keystore (Password: changeit)
openssl pkcs12 -export -in whatever-bundle.crt -inkey whatever_local.key -certfile whatever-bundle.crt -out whatever_keystore.p12
keytool -importkeystore -srckeystore whatever_keystore.p12 -srcstoretype pkcs12 -destkeystore whatever_keystore.jks -deststoretype JKS

## Truststore (Password: changeit)
keytool -import -file rootCa.crt -alias rootCa -keystore whatever_truststore.jks
```

## Self signed certificate

```shell
openssl req -x509 -newkey rsa:4096 -keyout localhost.key -out localhost.crt -days 3650 -nodes -subj /CN=something -extensions SAN
```

### Self signed for a list of hostnames

`reg.conf`

```conf
[req]
distinguished_name=req

[SAN]
subjectAltName = @alternate_names

[ alternate_names ]
DNS.1 = localhost
DNS.2 = localhost2
DNS.3 = localhost3
```

```shell
openssl req -x509 -newkey rsa:4096 -keyout localhost.key -out localhost.crt -days 3650 -nodes -subj /CN=something -extensions SAN -config reg.conf
```
