# Production Line

## Tipps

### Market Segments

- Produce for all 4 market segments (Basic, Mid, Expensive, Luxury)
    - 1/2 Luxury
    - 1/2 Expensive
    - 4/5 Mid
    - Basic is totally cheap, do in the end
- Luxury cars have all the features, Expensive have 2 priced less, Mid-range has 4-5 less, and Basic has minimum, 2-3 characteristics.

### Strategie

- To the beginning build 8-10 research center
    - First researches:
        1. Power (which is a lot cheaper)
        2. Music boxes
            - These can be upgraded to the first production line
            - Install it into the expensive and luxury cars
        3. Design center
- Build a production line without bottleneck (long operations with 2-3 workshop)
- After researchting other cars, and better production line, build one later. This line supports the possibility of collecting cars much faster, and extra features for the cars.
