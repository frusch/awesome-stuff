# theHunter Classic

Awesome list of links, tools, etc. around the game "theHunter classic".

## Interesting Forum Posts
- [The true cost of missions](https://forum.thehunter.com/viewtopic.php?f=31&t=91193)

## Sheets
- [Huntertable](https://docs.google.com/spreadsheets/d/1A5fkHBZ30arukzPY81_t7o6PFO9NFgku-9CEbP-HtrI) – A sheet to identify which animal can be shot with which weapon and on which maps it appears.
  - [Thread in theHunter Forum](https://forum.thehunter.com/viewtopic.php?f=31&t=76198)
- [Score Overview](https://docs.google.com/spreadsheets/d/194yLfQBYcsbNJHA58g70IA43vMd9oXo-Z488ct-XkgQ/) – A sheet to identify if the animale you are following or you are shot is a monster or not. This sheet shows you e.g. the max weight and score for every animal.

## Web-based Tools
- [Hunter Hinter](https://pastuh.github.io/hunterhinter/) – A webbased tool to find specific animals or weapons
  - [Thread in theHunter Forum](https://forum.thehunter.com/viewtopic.php?f=31&t=91563)
- [Mission-Item Tool](https://missionitemtool.herokuapp.com/) – A interactive tool to identify which missions you can do with you weapon arsenal and to track your mission progress

## Browser Extensions
- [theHunter:Classic helper](https://chrome.google.com/webstore/detail/thehunterclassic-helper/alfihijpbpajcmmhgcicnipjlinmmmle) – A Google Chrome Browserextension to show a better friendship list on the theHunter classic website among other things.

## Freemiums & Payable Options

### Old CD/DVD Versions

If you have an old theHunter classic CD or DVDs with unused Keys, you can activate them on this page: <https://www.thehunter.com/activation>

For example you can buy ["The Hunter - Pathfinder Starter Pack" on MMOGA](https://www.mmoga.com/Claves-de-Juegos/The-Hunter-Pathfinder-Starter-Pack.html) and active the key to get all the items from this pack.

## Animal timings

| Animal | Best hunting time |
|---|---|
| Sitka Deer | 7-8 am |
