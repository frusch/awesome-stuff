# Vim

## Interesting Articles/Tutorials

- [Everything you need to know to configure neovim using Lua](https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/)
- [Vim Adeventures (Game)](https://vim-adventures.com/)

## String manipulations

<kbd>s</kbd> Remove selected string and switch to insert mode

### To uppercase/lowercase

Select what you like to transform and then:

- Uppercase: <kbd>U</kbd>
- Lowercase: <kbd>u</kbd>

## Indentation

### Indent multiple lines

Select the lines with visual mode and press:

- <kbd>></kbd> for increasing the indentation
- <kbd><</kbd> for decreasing the indentation

### Decrease indentation

<kbd>Ctrl</kbd> + <kbd>D</kbd>

## Delete all lines of file

<kbd>gg</kbd> + <kbd>dG</kbd>

The <kbd>gg</kbd> moves the cursor to the beginning of the file.

The <kbd>d</kbd> is for the cut action and <kbd>G</kbd> is for moving the cursor to the end of the file. This way, <kbd>dG</kbd> cuts all the text from the current cursor position to the end of the file. This is why you need to be at the beginning of the file if you want to delete all the lines in Vim.


## Replace

### Replace all occurrences

```plain
:%s/<search_term>/<replace_term>/g
```

## Copy, Paste, Cut

### Between multiple vim instances

Copy: <kbd>"</kbd> + <kbd>+</kbd> + <kbd>yy</kbd>

Paste: <kbd>"</kbd> + <kbd>+</kbd> + <kbd>p</kbd>

## Move lines

### Up

<kbd>ddkP</kdb>

Explanation:

- <kbd>dd</kdb> will delete the line and add it to the default register
- <kbd>k</kdb> will move up a line
- <kbd>P</kdb> will paste above the current line

### Down

<kbd>ddp</kdb>

Explanation:

- <kbd>dd</kdb> will delete the line and add it to the default register
- <kbd>p</kdb> will paste below the current line

## Plugins

### nvim-tree

- <kbd>Ctrl</kbd> + <kbd>v</kbd> will open the file in a vertical split
- <kbd>Ctrl</kbd> + <kbd>x</kbd> will open the file in a horizontal split
- <kbd>Ctrl</kbd> + <kbd>t</kbd> will open the file in a new tab
- <kbd>Tab</kbd> will open the file as a preview (keeps the cursor in the tree)
