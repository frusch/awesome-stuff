# Spring Boot Snippets

## Logging

### Environment Variables

YAML:

```yaml
logging:
  level:
    org.springframework.core.env: DEBUG
```

K8s Env:

```yaml
- name: LOGGING_LEVEL_ORG_SPRINGFRAMEWORK_CORE_ENV
  value: DEBUG
```

### Authentication/Authorization

YAML:

```yaml
logging:
  level:
    org.springframework.security: DEBUG
```

K8s Env:

```yaml
- name: LOGGING_LEVEL_ORG_SPRINGFRAMEWORK_SECURITY
  value: DEBUG
```