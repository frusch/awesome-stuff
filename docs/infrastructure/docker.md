# Docker

## Show all images sorted by size

```shell
docker images --format "{{.Repository}} {{.Tag}} {{.ID}} {{.Size}}" | sort -k 4 -h | column -t -s' '
```
