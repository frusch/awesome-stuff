# Spring Boot - RabbitMQ

## Fanout Exchange

### Producer

`application.yaml`:

```yaml
spring:
  rabbitmq:
    host: rabbitmq-host-name
    username: username
    password: password
  cloud:
    stream:
      rabbit:
        bindings:
          someBinding-out-0:
            producer:
              exchangeType: fanout
      bindings:
        someBinding-out-0:
          destination: exchange-name
```

Somewhere in the sourcecode:

```kotlin
class SomeService(val streamBridge: StreamBridge) {
    fun someFunction() {
        ...
		streamBridge.send("someBinding-out-0", event)
    }
}
```

### Consumers

This could be copied for every consumer-group. You just have to change the `group` attribute in the `stream.bindings`.

`application.yaml`:

```yaml
spring:
  rabbitmq:
    host: rabbitmq-host-name
    username: username
    password: password
  cloud:
    stream:
      rabbit:
        bindings:
          someBinding-in-0:
            consumer:
              exchangeType: fanout
              autoBindDlq: true
              dlqTtl: 300000
              dlqDeadLetterExchange:
      bindings:
        someBinding-in-0:
          destination: exchange-name
          group: first-consumer
          consumer:
            concurrency: 1
            maxAttempts: 1
```

Somewhere in the sourcecode:

```kotlin
@Component
class EventListener {
	@Bean
	fun someBinding(): Consumer<Event> {
		return Consumer {
			logger.info { "Received $it" }
		}
	}
}
```
