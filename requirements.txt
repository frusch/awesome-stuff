# Documentation static site generator & deployment tool
mkdocs>=1.5.3

# Add your custom theme if not inside a theme_dir
# https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes
# https://github.com/squidfunk/mkdocs-material
mkdocs-material>=9.5.4

# Displaoys the last revision date of the current page based on Git 
# https://github.com/zhaoterryy/mkdocs-git-revision-date-plugin
mkdocs-git-revision-date-plugin>=0.3.2
